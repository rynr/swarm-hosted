swarm-hosted
============

The idea of this project is to setup a hosting environment for docker swarm without needing to open the swarm port to the internet.  
To achieve this, an internal VPN (yes, the VPN Port needs to be open) is created and the communication between the swarm nodes runs within this VPN.

status / next steps
-------------------

So far, docker is installed and a VPN server is created running on the `swarm_master`. The nodes do connect to the VPN.  
The docker swarm is initialized, but the nodes yet do not connect.

The next step is to have the nodes connect to the swarm master.  
Then an additional swarm configuration will be added to also run services on the cluster.

There's currently an issue that creation of the swarm does nor recognise if there was a swarm already created. The error message is `Can not create a new Swarm Cluster - This node is already part of a swarm`. This needs to be fixed soon as well.

requirements
------------

This setup relies on [`ansible`][ansible], which relies on [`python`][python]. Checkout the latest documentation on [Installing Ansible][ansible_install] if you are not famililar with it.

configure
---------

To configure this to your needs, change the [inventory][inventory] file to your needs and take care abot the authentication to the nodes.

You need one host in `swarm_master` and can have as many hosts in `swarm` as you like,

running
-------

Once your configuration is complete, run this command:

```sh
ansible-playbook -i inventory site.yml
```

[ansible]: https://ansible.com/
[ansible_install]: https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html
[inventory]: https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html
[python]: https://python.org/